import React from 'react'
import NavBar from '../components/NavBar'

const About = () => (
  <div>
  <NavBar>
  <div className="about">
      about page
  </div>
  <p>hey this is about page</p>
</NavBar>
<style jsx>{`
        p {
          color: blue;
        }
        div {
          background: red;
        }
        @media (max-width: 600px) {
          div {
            background: blue;
          }
        }
      `}</style>
  </div>
)

export default About
