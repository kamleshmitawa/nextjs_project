import React from 'react';
import Nav from './nav';

export default class NavBar extends React.Component {
    render() {
        return (
            <div>
                <Nav />
                <div>
                    {this.props.children}
                </div>
            </div>
        )
    }
}